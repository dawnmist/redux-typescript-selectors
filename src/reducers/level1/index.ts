import { combineReducers } from 'redux';
import { Action, ActionType } from 'src/actions';
import { Slicer } from 'src/reducers';
import level2, * as fromLevel2 from './level2';

/**
 * Reducers
 */
export interface Level1State {
  level2: fromLevel2.Level2State;
  pageNum: number;
}

const pageNum = (state: number = 1, action: Action): number =>
  action.type === ActionType.SetPageNum ? action.pageNum : state;

export default combineReducers<Level1State>({
  level2,
  pageNum
});

/**
 * Selectors
 */
const level1To2Slicer: Slicer<Level1State, fromLevel2.Level2State> = (state: Level1State) =>
  state.level2;

export const getSelectors = <S extends object>(slicer: Slicer<S, Level1State>) => ({
  ...fromLevel2.getSelectors((parentState: S) => level1To2Slicer(slicer(parentState))),
  getPageNum: (parentState: S) => slicer(parentState).pageNum
});
