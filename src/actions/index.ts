import * as fromUuid from 'uuid';

export const enum ActionType {
  CreateMessage = 'CREATE_MESSAGE',
  SetPageNum = 'SET_PAGE_NUMBER',
  SetNumPerPage = 'SET_NUM_PER_PAGE'
}

export interface CreateMessage {
  type: ActionType.CreateMessage;
  id: string;
  message: string;
}

export const createMessage = (message: string): CreateMessage => ({
  type: ActionType.CreateMessage,
  id: fromUuid.v4(),
  message
});

export interface SetPageNum {
  type: ActionType.SetPageNum;
  pageNum: number;
}

export const setPageNum = (pageNum: number): SetPageNum => ({
  type: ActionType.SetPageNum,
  pageNum
});

export interface SetNumPerPage {
  type: ActionType.SetNumPerPage;
  numPerPage: number;
}

export const SetNumPerPage = (numPerPage: number): SetNumPerPage => ({
  type: ActionType.SetNumPerPage,
  numPerPage
});

export type Action = CreateMessage | SetNumPerPage | SetPageNum;
