import { combineReducers } from 'redux';
import level1, * as fromLevel1 from './level1';
import numPerPage, * as fromNumPerPage from './numPerPage';

export type Slicer<S extends object, T> = (state: S) => T;

export interface State {
  level1: fromLevel1.Level1State;
  numPerPage: fromNumPerPage.NumPerPageState;
}

export default combineReducers<State>({
  level1,
  numPerPage
});

export const selectors = {
  ...fromLevel1.getSelectors((state: State) => state.level1),
  ...fromNumPerPage.getSelectors((state: State) => state.numPerPage)
};
