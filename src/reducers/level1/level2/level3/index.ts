import { combineReducers } from 'redux';
import { Action, ActionType } from 'src/actions';
import { Slicer } from 'src/reducers';

/**
 * Reducers
 */
export interface MessageById {
  [id: string]: string;
}

export interface Level3State {
  messageById: MessageById;
  messageIds: string[];
}

const messageById = (state: MessageById = {}, action: Action): MessageById => {
  switch (action.type) {
    case ActionType.CreateMessage:
      return { ...state, [action.id]: action.message };
    default:
      return state;
  }
};

const messageIds = (state: string[] = [], action: Action): string[] => {
  switch (action.type) {
    case ActionType.CreateMessage:
      return [...state, action.id];
    default:
      return state;
  }
};

export default combineReducers<Level3State>({
  messageById,
  messageIds
});

/**
 * Selectors
 */
const getMessages = <S extends object>(slicer: Slicer<S, Level3State>) => (
  parentState: S,
  start?: number,
  count?: number
): string[] => {
  const state = slicer(parentState);
  const messages = state.messageIds.map((id) => state.messageById[id]);
  return start !== undefined && count !== undefined ? messages.slice(start, count) : messages;
};

export const getSelectors = <S extends object>(slicer: Slicer<S, Level3State>) => ({
  getMessageById: (parentState: S, id: string) => slicer(parentState).messageById[id] || null,
  getMessagesById: (parentState: S) => slicer(parentState).messageById,
  getMessageIds: (parentState: S) => slicer(parentState).messageIds,
  getMessages: getMessages(slicer)
});
