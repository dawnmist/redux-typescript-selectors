import { combineReducers } from 'redux';
import { Action, ActionType } from 'src/actions';
import { Slicer } from 'src/reducers';
import level3, * as fromLevel3 from './level3';

/**
 * Reducers
 */
export interface Level2State {
  hasMessages: boolean;
  level3: fromLevel3.Level3State;
}

const hasMessages = (state: boolean = false, action: Action): boolean =>
  action.type === ActionType.CreateMessage ? true : state;

export default combineReducers<Level2State>({
  hasMessages,
  level3
});

/**
 * Selectors
 */
const level2To3Slicer: Slicer<Level2State, fromLevel3.Level3State> = (state: Level2State) =>
  state.level3;

export const getSelectors = <S extends object>(slicer: Slicer<S, Level2State>) => ({
  ...fromLevel3.getSelectors((parentState: S) => level2To3Slicer(slicer(parentState))),
  getHasMessages: (parentState: S) => slicer(parentState).hasMessages
});
