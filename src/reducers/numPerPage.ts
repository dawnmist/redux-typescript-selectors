import { Action, ActionType } from 'src/actions';
import { Slicer } from 'src/reducers';

/**
 * Reducers
 */
export type NumPerPageState = number;

export default (state: number = 5, action: Action): number =>
  action.type === ActionType.SetNumPerPage ? action.numPerPage : state;

/**
 * Selectors
 */
export const getSelectors = <S extends object>(slicer: Slicer<S, number>) => ({
  getNumPerPage: (parentState: S) => slicer(parentState)
});
