import { ActionType, CreateMessage } from 'src/actions';
import { Slicer } from 'src/reducers';
import reducer, { getSelectors, Level3State } from './index';

/** Create a slicer function that simply returns what it was given */
const selfSlicer: Slicer<Level3State, Level3State> = (state) => state;

/** And then initialize the selectors object with that slicer */
const selectors = getSelectors(selfSlicer);

describe('Level 3 Reducer Tests', () => {
  it('Should process a create message action', () => {
    const action: CreateMessage = {
      type: ActionType.CreateMessage,
      id: 'abc123',
      message: 'A test message'
    };
    const expectedState: Level3State = {
      messageById: { abc123: 'A test message' },
      messageIds: ['abc123']
    };

    const result = reducer(undefined, action);
    expect(result).toBeDefined();
    expect(result).toEqual(expectedState);
  });
});

describe('Level 3 Selector Tests', () => {
  it('Should be able to retrieve a message by id', () => {
    const state: Level3State = {
      messageById: { abc123: 'A test message' },
      messageIds: ['abc123']
    };

    const result = selectors.getMessageById(state, 'abc123');
    expect(result).toBeDefined();
    expect(result).not.toBeNull();
    expect(result).toBe('A test message');
  });
});
