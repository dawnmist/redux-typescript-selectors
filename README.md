# Strongly Typed, Fully Encapsulated Redux Selectors in TypeScript

This repository has been put together to demonstrate a way to create nested,
fully encapsulated redux selectors with strongly typed TypeScript selectors.
The "parent" or "root" reducer does not need to create an exported function
for every individual selector function defined within it's child reducers,
greatly reducing the amount of repetition needed.

## Inspiration

The main thing that I haven't been happy with when working with React+Redux has
been the amount of repetition involved in defining the selectors. So when I was
in the process of starting a new project, I wanted to find a usable solution
that didn't lose type-safety. Some of the more interesting articles that I read
while researching this included:

* [Scaling your Redux App with ducks](https://medium.freecodecamp.org/scaling-your-redux-app-with-ducks-6115955638be)
* [Redux modules and code-splitting](http://nicolasgallagher.com/redux-modules-and-code-splitting/)
* [How to use Redux on highly scalable javascript applications?](https://medium.com/@alexmngn/how-to-use-redux-on-highly-scalable-javascript-applications-4e4b8cb5ef38)
* [Description of a possible utility 'shiftSelectors'](https://stackoverflow.com/questions/41274439/redux-colocating-selectors-with-reducers/41860269#41860269)
* [Redux utility to bind selectors to a slice of state](https://gist.github.com/jslatts/1c5d4d46b6e5b0ac0e917fa3b6f7968f) (possible implementation of 'shiftSelectors' above).

In particular, the utility functions for rewrapping the child reducer selector
functions with the parent reducer's state slicing function looked a lot like
what I was looking for...but I couldn't find any way with TypeScript 2.8 to
write a type definition for that utility function that didn't lose the type
information for the child's function parameters. It also lost how many
parameters the child's selector function had. Losing all IDE hinting or
parameter type safety wasn't an acceptable cost.

After thinking about it for a while longer, I realised that the only reason
that the child selectors get wrapped by the parent was because the child
doesn't know (and shouldn't have to know) where it is mounted in the state
tree - which means that it cannot retrieve itself from the overall state.
Likewise, in order to keep as much of the child reducer's information
encapsulated _in_ the child reducer's definitions, the parent reducer shouldn't
have to know every selector function that the child reducer provides. If I was
willing to have selectors be defined in an object similar to the utility
function defined in the gist above, there was no reason that the _parent_
reducer needed to be the one defining the function returns...if the parent
instead gave a slicing function _to_ the child reducer when asking for its list
of selector functions, the child could define those functions directly terms of
the _parent's_ state and simply apply the slicing function itself when a
selector function was called. This then means that each of the selector
functions don't have to be repeated for every step up the state tree...and that
the selectors to access a reducer's state data is solely encapsulated within the
same reducer as where the state data resides.

## How it works

There are two main type definitions required to do this. The first is the type
definition for the Slicer:

```javascript
export type Slicer<ParentState extends object, ChildState> = (state: ParentState) => ChildState;
```

The second is the type for defining a function to _create_ selector functions
based on the parent reducer's state definition, for example:

```javascript
export const getSelectors = <ParentState extends object>(slicer: Slicer<ParentState, ChildState>) => ({

  // Simple functions that simply take the parent state, apply the slicer function provided by the parent,
  // and then retrieve the desired data from the child state.
  getMessagesById: (parentState: ParentState) => slicer(parentState).messageById,
  getMessageIds: (parentState: ParentState) => slicer(parentState).messageIds,

  // Function that takes a second parameter, defined in terms of the parent's state type and the id,
  // that then applies the given slicer.
  getMessageById: (parentState: ParentState, id: string) => slicer(parentState).messageById[id] || null,

  // Can also define functions either that take the slicer itself and return a function
  getMessages: getMessages(slicer),

  // Or that apply the slicer and send just the sliced state into the function
  getMessageCount: (parentState: S) => getMessageCount(slicer(parentState))

});

const getMessages = <ParentState extends object>(slicer: Slicer<ParentState, ChildState>) =>
  (parentState: ParentState) => {
    const state = slicer(parentState);
    return Object.keys(state.messageIds).map((id) => state.messageById[id]);
  }
  
const getMessageCount = (state: ChildState) => state.messageIds.length;
```

The advantage of defining the selector function as taking the slicer and
returning a function that applies the slicer internally is that the list of
function parameters is only defined in the one place. The disadvantage
is that if the function needs to be able to be used within the current reducer
as well you may need to create a 'self-slicer' function that simply returns
the object it was given:

```javascript
const selfSlicer: Slicer<State, State> = (state) => state;

const getPageNum = <S extends object>(slicer: Slicer<S, State>) => (parentState: S) =>
  slicer(parentState).pageNum;

const getNumPerPage = <S extends object>(slicer: Slicer<S, State>) => (parentState: S) =>
  slicer(parentState).numPerPage;

const getMessages = <S extends object>(slicer: Slicer<S, State>) => (parentState: S) => {
  const state = slicer(parentState);
  return Object.keys(state.ids).map((id) => state.byId[id]);
};

const getPagedMessages = <S extends object>(slicer: Slicer<S, State>) => (parentState: S) => {
  const state = slicer(parentState);
  const pageNum = getPageNum(selfSlicer)(state);
  const numPerPage = getNumPerPage(selfSlicer)(state);
  const messages = getMessages(selfSlicer)(state);
  const start = (pageNum - 1) * numPerPage;
  return messages.slice(start, start + numPerPage);
});
```

A selector could then be further nested by a reducer defining an intermediate
Slicer function that wraps the state result from its own parent's slicer
function, so that the child reducer creates selectors based on its grandparent's
state shape. This can then be combined with its own selectors:

```javascript
const level1To2Slicer: Slicer<Level1State, fromLevel2.Level2State> = (state: Level1State) =>
  state.level2;

export const getSelectors = <S extends object>(slicer: Slicer<S, Level1State>) => ({

  // Child's selectors - wrapping the parent slicer inside the level1To2Slicer.
  ...fromLevel2.getSelectors((parentState: S) => level1To2Slicer(slicer(parentState))),
  
  // This reducers selectors
  getPageNum: (parentState: S) => slicer(parentState).pageNum

});
```

Finally, the root reducer itself can export the tree of selectors by using the
single getSelectors function defined in each of its children reducers:

```javascript
export default combineReducers<State>({
  level1,
  numPerPage
});

export const selectors = {
  ...fromLevel1.getSelectors((state: State) => state.level1),
  ...fromNumPerPage.getSelectors((state: State) => state.numPerPage)
};
```

You may also choose to create a selector object for each major tree of the root
state (e.g. when you expect that components will usually only need one tree of
the root reducer, so that they don't need to import the entire set of selectors):

```javascript
export default combineReducers<State>({
  level1,
  numPerPage
});

export const level1Selectors =
  fromLevel1.getSelectors((state: State) => state.level1);

export const numPerPageSelectors =
  fromNumPerPage.getSelectors((state: State) => state.numPerPage);
```

This example repository demonstrates a reducer tree 4 levels deep, that is able
to create selectors based on the root reducer's overall state.

## Testing selectors

In order to create the set of selectors for writing tests for them, a self-
slicer function can be created that simply takes the original state of the
reducer and returns the same object. This allows testing the behaviour of
a single reducer without respect to where it is located in the state tree:

```javascript
import { Slicer } from 'src/redux';
import { getSelectors, State } from '.';

const selfSlicer: Slicer<State, State> = (state) => state;
const selectors = getSelectors(selfSlicer);
```

## Benefits

* Scalable. One of the issues with the more traditional method of wrapping
  a child's selectors in the parent reducer results in all the selectors being
  repeated in every parent/grandparent/and so-on up to the root reducer. By the
  time the app has grown to a significant size, the list of selectors in the
  root reducer has often become huge.
* Ability to move a reducer around, without having to rewrite all the wrapping
  selector functions.
* Better code completion inside tools like VSCode. With the traditional method
  of declaring selectors, it is very common that the editor will guess the
  child reducer file as the required import to provide a selector, which then
  gives errors because the child reducer doesn't take the whole state. With this
  method, only the root-level actually has selectors to be proposed for import.
* Theoretically it should make it a lot easier to do code-splitting or to
  reorganize the structure of the application so that reducers can be
  registered on demand. It should enable you to easily reorganize the
  structure of the app so that all items relating to a single feature can be
  stored together in a feature-type organization instead of a file-type
  organization. The main thing missing in this particular example for doing
  that is some way to register the reducer dynamically - have a look at
  Nicolas Gallagher's Reducer Registry for one example of how to do this.
  Note: I say theoretically, as I have not personally tested registering
  reducers on demand yet.

## Penalties

* Due to the selectors being exported through an object rather than as
  individual functions, static analysis of the function tree is not able to
  separate out unused selector functions for removal (no Tree-Shaking of your
  reducer selector functions).
* Files import more of the selectors than they necessarily need. This can be
  reduced by doing things like exporting different root-level selector objects
  for different trees of the reducer state rather than a single global one.
* It is exceedingly easy to create a circular dependency in react-native if used
  with react-navigation. Since the reducer function definition depends on the
  react-navigation component stack definition, that function must be separated
  into a different file from the reducer selector functions, and the root
  reducer definition likewise must not be kept in the same file as the root
  selector object. If they are defined in the same files then you get the case
  where a component that _uses_ the selector function and the stack definition
  can end up trying to decompose the selector object before it has actually been
  fully created (when the fields are still undefined instead of containing the
  selector functions) and trying to call the undefined value as a function.
