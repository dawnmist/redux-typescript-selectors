import * as React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from 'src/reducers';
import './App.css';

import logo from './logo.svg';

const store = createStore(rootReducer);

export default class App extends React.Component {
  public render() {
    return (
      <Provider store={store}>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>
        </div>
      </Provider>
    );
  }
}
